package com.vti.vtiacademy.modal.entity;

public enum Role {
    ADMIN, MENTOR, TUTOR, STUDENT
}
