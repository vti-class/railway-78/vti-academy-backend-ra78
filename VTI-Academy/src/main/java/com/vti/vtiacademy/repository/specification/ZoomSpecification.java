package com.vti.vtiacademy.repository.specification;

import com.vti.vtiacademy.modal.dto.ClassRoomSearch;
import com.vti.vtiacademy.modal.dto.ZoomSearchRequest;
import com.vti.vtiacademy.modal.entity.ClassRoom;
import com.vti.vtiacademy.modal.entity.Zoom;
import com.vti.vtiacademy.utils.Utils;
import org.springframework.data.jpa.domain.Specification;

public class ZoomSpecification {

    public static Specification<Zoom> buildCondition(ZoomSearchRequest request){
        return Specification.where(buildConditionName(request));
    }

    public static Specification<Zoom> buildConditionName(ZoomSearchRequest request){
        if(!Utils.isBlank(request.getName())){
            return (root, query, cri) -> {
                return cri.like(root.get("name"), "%" + request.getName() + "%");
            };
        }
        return null;
    }

    // Tạo thêm điều kiện để tìm kiếm
}
