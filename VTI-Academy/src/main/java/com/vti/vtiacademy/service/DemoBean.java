package com.vti.vtiacademy.service;

import lombok.Data;

@Data
public class DemoBean {
    private String name;
    private int count;
}
