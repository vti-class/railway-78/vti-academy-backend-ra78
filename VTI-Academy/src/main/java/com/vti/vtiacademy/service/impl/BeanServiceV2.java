package com.vti.vtiacademy.service.impl;

import com.vti.vtiacademy.service.IBeanService;
import org.springframework.stereotype.Component;

@Component
public class BeanServiceV2 implements IBeanService {
    @Override
    public void print() {
        System.out.println("Phiên bản 2");
    }
}
