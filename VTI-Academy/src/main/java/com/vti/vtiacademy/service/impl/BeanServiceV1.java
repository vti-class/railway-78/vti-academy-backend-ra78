package com.vti.vtiacademy.service.impl;

import com.vti.vtiacademy.service.IBeanService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

//@Service
//@Repository
@Component
@Primary
public class BeanServiceV1 implements IBeanService {
    @Override
    public void print() {
        System.out.println("Phiên bản 1");
    }
}
