package com.vti.vtiacademy.config;

import com.vti.vtiacademy.service.DemoBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class DemoBeanConfig {
    // Trong file này chứa các method để custom các giá trị trong bean
    @Bean
    public DemoBean demoBeanV1(){
        DemoBean demoBean = new DemoBean();
        demoBean.setName("Demo beans");
        demoBean.setCount(1);
        return demoBean;
    }

    @Bean
    @Primary
    public DemoBean demoBeanV2(){
        DemoBean demoBean = new DemoBean();
        demoBean.setName("Demo beans V2");
        demoBean.setCount(1);
        return demoBean;
    }
}
